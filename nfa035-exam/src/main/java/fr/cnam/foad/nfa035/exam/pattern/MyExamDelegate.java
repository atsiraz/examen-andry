package fr.cnam.foad.nfa035.exam.pattern;

import fr.cnam.foad.nfa035.exam.model.MyExam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component(value = "exam-encours")
public class MyExamDelegate implements MyExamDelegateContract{

    @Autowired
    private MyExam exam;


    @Override
    public String delegateToString() {
        return exam.toString();
    }
}
