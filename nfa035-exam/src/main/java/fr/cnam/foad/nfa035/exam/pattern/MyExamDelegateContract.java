package fr.cnam.foad.nfa035.exam.pattern;


public interface MyExamDelegateContract {

    String delegateToString();

}
