package fr.cnam.foad.nfa035.exam.pattern;

import fr.cnam.foad.nfa035.exam.model.MyExam;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MyExamFactory
{

    @Bean("exam-en-cours")
    public MyExam getCurrentExam(){
        return new MyExam("Bibliotheques et Patterns", "NFA035", "CNAM", "Ile de France", new Date());
    }

}
